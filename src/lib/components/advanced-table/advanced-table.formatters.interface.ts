import {
  ActionLinkFormatter, ButtonFormatter, DateTimeFormatter, LanguageFormatter,
  LinkFormatter,
  NestedPropertyFormatter, NgClassFormatter, SelectRowFormatter,
  TemplateFormatter, TranslationFormatter,
  TrueFalseFormatter, ActionButtonFormatter, ActionSplitButtonFormatter,
} from './advanced-table.formatters';

export interface IAdvancedTableFormatters {
  ActionLinkFormatter?: ActionLinkFormatter;
  ButtonFormatter?: ButtonFormatter;
  DateTimeFormatter?: DateTimeFormatter;
  LanguageFormatter?: LanguageFormatter;
  LinkFormatter?: LinkFormatter;
  NestedPropertyFormatter?: NestedPropertyFormatter;
  NgClassFormatter?: NgClassFormatter;
  SelectRowFormatter?: SelectRowFormatter;
  TemplateFormatter?: TemplateFormatter;
  TranslationFormatter?: TranslationFormatter;
  TrueFalseFormatter?: TrueFalseFormatter;
  ActionButtonFormatter?: ActionButtonFormatter;
  ActionSplitButtonFormatter?: ActionSplitButtonFormatter;
}
export const FORMATTERS: IAdvancedTableFormatters = {
      ActionLinkFormatter: new ActionLinkFormatter(),
      ButtonFormatter: new ButtonFormatter(),
      DateTimeFormatter: new DateTimeFormatter(),
      LanguageFormatter: new LanguageFormatter(),
      LinkFormatter: new LinkFormatter(),
      NestedPropertyFormatter: new NestedPropertyFormatter(),
      NgClassFormatter: new NgClassFormatter(),
      SelectRowFormatter: new SelectRowFormatter(),
      TemplateFormatter: new TemplateFormatter(),
      TranslationFormatter: new TranslationFormatter(),
      TrueFalseFormatter: new TrueFalseFormatter(),
      ActionButtonFormatter: new ActionButtonFormatter(),
      ActionSplitButtonFormatter: new ActionSplitButtonFormatter(),
};
