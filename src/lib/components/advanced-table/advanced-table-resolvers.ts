import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedFormsService} from '@universis/forms';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AdvancedTableConfigurationResolver implements Resolve<string> {
    constructor(private _http: HttpClient) {}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<any>|any {
        // get list
        const list = route.data.list || route.params.list;
        // get model
        const model = route.data.model || route.params.model;
        // get configuration
        return this._http.get(`assets/lists/${model}/${list}.json`).toPromise();
    }
}

@Injectable()
export class AdvancedSearchConfigurationResolver implements Resolve<string> {
    constructor(private _http: HttpClient) {}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<any>|any {
        // get list
        const list = route.data.list || route.params.list;
        // get model
        const model = route.data.model || route.params.model;
        // get configuration
        return this._http.get(`assets/lists/${model}/search.${list}.json`).toPromise();
    }
}
