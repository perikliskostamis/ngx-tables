import { Component, OnInit, OnDestroy, Input, EventEmitter, ViewEncapsulation, OnChanges, SimpleChanges, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-column-selector',
  templateUrl: './column-selector.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
    .form-select {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        padding: 5px;
    }
    .form-select * {
        padding: 5px 5px;
    }
    .btn.disabled, .btn:disabled {
        border-color: transparent !important;
    }
    `
  ]
})
export class ColumnSelectorComponent extends RouterModalOkCancel implements OnInit, OnDestroy, OnChanges {
    
    @Input() columns: { key: string, title: string, visible: boolean, index: number }[];
    public dismiss: EventEmitter<any> = new EventEmitter();

    @Input() selectedColumns: { key: string, title: string }[];

    @ViewChild('moveUpButton') moveUpButton: ElementRef;
    @ViewChild('moveDownButton') moveDownButton: ElementRef;

    availableColumnsControl = new FormControl();
    selectedColumnsControl = new FormControl();

    ok(): Promise<any> {
        this._modalRef.hide();
        this.applyChanges();
        this.dismiss.emit('ok');
        return Promise.resolve('ok');
    }
    cancel(): Promise<any> {
        this._modalRef.hide();
        this.dismiss.emit('cancel');
        return Promise.resolve('cancel');
    }

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _modalRef: BsModalRef,
    private ref: ChangeDetectorRef,
    private _translateService: TranslateService) { 
        super(router, activatedRoute);
    }
    ngOnChanges(changes: SimpleChanges): void {
        //
    }
    ngOnDestroy(): void {
        //
    }
    ngOnInit(): void {
        if (Array.isArray(this.columns)) {
            this.selectedColumns = this.columns.filter((item) => {
                return item.visible;
            }).sort((a, b) => {
                if (a.index < b.index) {
                    return -1
                }
                if (a.index > b.index) {
                    return 1;
                }
                return 0;
            }).map((item) => {
                return {
                    key: item.key,
                    title: item.title
                }
            });
        }
    }

    private applyChanges() {
        let moveIndex = this.selectedColumns.length - 1;
        for (const column of this.columns) {
            const findIndex = this.selectedColumns.findIndex((item) => {
                return item.key === column.key;
            });
            if (findIndex < 0) {
                column.visible = false;
                moveIndex += 1;
                column.index = moveIndex;
            } else {
                column.visible = true;
                column.index = findIndex;
            }
        }
    }

    removeColumns() {
        const selected = this.selectedColumnsControl.value as string[];
        if (selected.length > 0) {
            for (const value of selected) {
                let findIndex = this.selectedColumns.findIndex((item) => {
                    return item.key === value;
                });
                if (findIndex >= 0) {
                    this.selectedColumns.splice(findIndex, 1);
                }
            }
            // reset column
            this.selectedColumnsControl.reset();
        }
    }

    addColumns() {
        const selected = this.availableColumnsControl.value as string[];
        if (selected.length > 0) {
            for (const value of selected) {
                let findColumn = this.columns.find((item) => {
                    return item.key === value;
                });
                if (findColumn) {
                    const findIndex = this.selectedColumns.findIndex((item) => {
                        return item.key === value;
                    });
                    if (findIndex < 0) {
                        this.selectedColumns.push({
                            key: findColumn.key,
                            title: findColumn.title
                        });
                    }
                }
            }
            // find the last index of the selected items
            const lastSelectedIndex = this.columns.findIndex((item) => {
                return item.key === selected[selected.length - 1];
            });
            // reset control
            this.availableColumnsControl.reset();
            if (lastSelectedIndex > -1 && lastSelectedIndex < this.columns.length - 1) {
                this.availableColumnsControl.patchValue([
                    this.columns[lastSelectedIndex + 1].key
                ])
            }
        }
    }

    moveUp() {
        this.moveUpButton.nativeElement.disabled = true;
        const selected = this.selectedColumnsControl.value as string[];
        if (selected.length > 0) {
            for (const value of selected) {
                const findIndex = this.selectedColumns.findIndex((item) => {
                    return item.key === value;
                });
                if (findIndex > 0) {
                    const column = this.selectedColumns[findIndex];
                    this.selectedColumns.splice(findIndex, 1);
                    this.selectedColumns.splice(findIndex - 1 , 0, column);
                }
            }
        }
        this.moveUpButton.nativeElement.disabled = false;
    }

    moveDown() {
        this.moveDownButton.nativeElement.disabled = true;
        const selected = this.selectedColumnsControl.value as string[];
        if (selected.length > 0) {
            for (const value of selected) {
                let findIndex = this.selectedColumns.findIndex((item) => {
                    return item.key === value;
                });
                const length = this.columns.length;
                if (findIndex >= 0 && findIndex < length) {
                    const column = this.selectedColumns[findIndex];
                    this.selectedColumns.splice(findIndex, 1);
                    this.selectedColumns.splice(findIndex + 1 , 0, column);
                }
            }
        }
        this.moveDownButton.nativeElement.disabled = false;
    }
  

}
