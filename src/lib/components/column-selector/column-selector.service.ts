import { Injectable } from '@angular/core';
import { ModalService } from '@universis/common';
import { ColumnSelectorComponent } from './column-selector.component';
import { cloneDeep } from 'lodash';

@Injectable()
export class ColumnSelectorService {

  constructor(private _modal: ModalService) {
    //
  }

  selectColumns(columns: { key: string, title: string, visible: boolean, index: number }): 
    Promise<{result: string, columns?: { key: string, title: string, visible: boolean, index: number }[]}> {
    return new Promise((resolve, reject) => {
      try {
        const modalRef = this._modal.openModalComponent(ColumnSelectorComponent, {
          class: 'modal-lg modal-table',
          ignoreBackdropClick: true,
          animated: true,
          initialState: {
            columns: columns,
          }
        });
        const componentRef: ColumnSelectorComponent = modalRef.content;
        componentRef.dismiss.subscribe((result: string) => {
          if (result === 'ok') {
            return resolve({
              result: result,
              columns: componentRef.columns
            });
          }
          return resolve({
            result: result
          });
          
        });
      } catch (err) {
        return reject(err);
      }
    });
  }

}
