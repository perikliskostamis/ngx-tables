export const TABLES_EN = {
    "Tables": {
      "Export": "Export",
        "More": "More",
        "Search": "Search",
        "SearchPlaceholder": "Search...",
        "GroupBy": "Group by",
        "NotDefined": "Not defined",
        "DataTable": {
            "paginate": {
                "first": "First",
                "last": "Last",
                "next": "Next",
                "previous": "Previous"
            },
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "Showing 0 to 0 of 0 entries",
            "processing": "Processing data...",
            "emptyTable": "There are no matching records."
        },
      "ColumnDisplay": "Column Display",
      "ColumnDisplayInfo": "Επιλέξτε τις στήλες που θέλετε να εμφανίζονται.",
      "Save": "Save",
      "Apply": "Apply",
      "Cancel": "Cancel",
      "RecordsPerPage": "Items per page",
      "PageInfo": "{{start}}-{{end}} of {{recordsTotal}}",
      "SearchListPlaceHolder": "Search in list...",
      "Filters": "Filters",
      "All": "All",
      "AddItem": "Add item",
      "EmptyTableTitle": "There are no matching records.",
      "EmptyTableMessage": "Please try to use other keywords or search parameters.",
      "RemoveItem": "Remove item",
      "Items": "items",
      "Item": "item",
    "AddItemMessage": {
      "title": "Add item",
      "one": "A new item has been added successfully.",
      "many": "{{value}} new items have been added successfully."
    },
    "RemoveItemMessage": {
      "title": "Remove items",
      "one": "An item has been successfully removed.",
      "many": "{{value}} items have been successfully removed."
    },
    "RemoveItemsTitle": "Remove items",
    "RemoveItemsMessage": "You are going to remove one or more instructor courses. Do you want to proceed?",
      "SelectAll": "Select all",
      "ClearSelection": "Clear selection",
      "Actions": {
        "Label": "Edit",
        "Start": "Start",
        "ActionMessage": {
          "None": "There is no selected item that meets action requirements.",
          "One": "You have selected one item that meets action requirements.",
          "Many": "You have selected {{length}} items that meet action requirements."
        },
        "CompletedWithErrors": {
          "Title": "The operation was completed with errors.",
          "Description": {
            "One": "One item has not been updated due to an error occurred while executing process.",
            "Many": "{{errors}} items have not been updated due to an error occurred while executing process."
          }
        },
        "CompletedWithSuccess": {
          "Title": "The operation was completed successfully."
        }
      },
      "SelectColumns": "Select columns",
      "MoveUp": "Move up",
      "MoveDown": "Move down",
      "AddColumn": "Add",
      "RemoveColumn": "Remove",
      "SelectColumnHelp": "Select one or more columns and press 'Add' button to include them in table. Use 'Remove' button to exclude one or more columns from view.",
      "MoveColumnHelp": "Use 'Move up' and 'Move down' buttons to change column order.",
      "UnknownColumn": "Unknown",
      "SelectColumn": "Select",
      "ActionLinkColumn": "Actions"
    },
    "TrueFalse": {
        "long": {
            "Yes": "Yes",
            "No": "No"
        }
    },
    "Settings": {
      "OperationCompleted": "The operation was completed successfully."
    },
    "AdvancedSearch": {
        "Choose": "<Select a saved search>",
        "ChooseDate": "Choose date",
        "SavedSearches": "Saved searches",
        "ToggleDropdown" : "Toggle Dropdown",
        "SaveCurrentSearch": "Save search",
        "SaveAs" : "Save as...",
        "Save" : "Save",
        "Delete" : "Remove",
        "More": "More",
        "Less": "Less",
        "Modal" : {
            "Title" : "Save current search criteria",
            "Help" : "Please add a title for the given search criteria. This title will help you to use those criteria again in the future.",
            "TitleOnUserError" : "Please use a valid name for your list",
            "TitleOnListAlreadyCreated" : "Name already in use. Please choose a different one",
            "NoListDefined" : "Cannot define user storage key.",
            "EmptyCriteriaList" : "Please define search criteria before save.",
            "SaveNameLabel" : "List name",
            "SaveNamePlaceholder" : "Type a title here...",
            "Accept" : "Save",
            "Decline" : "Decline",
            "Close" : "Close"
        }
    },
  "AddItems" : {
    "SelectItems": "Select items",
  }
}
