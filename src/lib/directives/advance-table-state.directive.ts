import {
    Directive, Optional, Input, OnInit,
} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { NgxTablesState } from '../reducers';
import { AdvancedTableComponent } from '../components/advanced-table/advanced-table.component';
import * as fromTables from '../reducers';
import { first } from 'rxjs/operators';
import { AppendState } from '../actions/table.actions';
import { cloneDeep, values } from 'lodash';

@Directive(
    {
        // tslint:disable-next-line: directive-selector
        selector: '[tableState]',
    },
)
export class AdvancedTableState implements OnInit {

    @Input() tableState: string;

    constructor(private table: AdvancedTableComponent,
        @Optional() private store: Store<NgxTablesState>) {
        
    }
    ngOnInit(): void {
        if (this.store == null) {
            return;
        }
        this.table.stateSave = true;
        this.table.stateLoadCallback = (settings, callback) => {
            this.store.pipe(select(fromTables.selectTable, { id: this.tableState }), first()).subscribe((state: any) => {
                return callback(state && state.table);
            });
        }
        let firstDraw = true;
        this.table.draw.subscribe(() => {
            if (firstDraw === false) {
                return;
            }
            if (this.table.dataTable == null) {
                return;
            }
            firstDraw = false;
            const loadeStateSettings: {
                oSavedState?: {
                    ColReorder?: number[];
                    scroller: {
                        topRow: number,
                        baseScrollTop: number,
                        baseRowTop: number
                    }
                }
            } = this.table.dataTable.settings()[0];
            const scrollerSettings = loadeStateSettings.oSavedState &&
                loadeStateSettings.oSavedState.scroller;
            if (scrollerSettings && scrollerSettings.topRow) {
                const scroller = this.table.dataTable.scroller();
                scroller.scrollToRow(scrollerSettings.topRow, true);
            }
        });
        this.table.stateSaveCallback = (settings, data) => {
            this.store.dispatch(new AppendState(this.tableState, data));
        }
    }
}