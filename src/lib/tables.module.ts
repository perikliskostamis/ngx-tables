import {NgModule, OnInit, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedTableComponent, COLUMN_FORMATTERS} from './components/advanced-table/advanced-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from './components/advanced-table/advanced-table-search.component';
import { DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {BtnExpDirective} from './directives/btnexp.directive';
import {AdvancedTableSettingsComponent} from './components/advanced-table/advanced-table-settings.component';
import {SharedModule, TemplatePipe} from '@universis/common';
import { AdvancedTableModalBaseComponent } from './components/advanced-table-modal/advanced-table-modal-base.component';
import {RouterModalModule} from '@universis/common/routing';
import { AdvancedSearchFormComponent } from './components/advanced-search-form/advanced-search-form.component';
import {AdvancedFormsModule} from '@universis/forms';
import {FormioModule} from 'angular-formio';
import { AdvancedListComponent } from './components/advanced-list/advanced-list.component';
import {RouterModule} from '@angular/router';
import {
    AdvancedSearchConfigurationResolver,
    AdvancedTableConfigurationResolver
} from './components/advanced-table/advanced-table-resolvers';
import {AdvancedRowActionComponent} from './components/advanced-row-action/advanced-row-action.component';
import {FORMATTERS} from './components/advanced-table/advanced-table.formatters.interface';

import {TABLES_EL} from './i18n/tables.el';
import {TABLES_EN} from './i18n/tables.en';
import { TableSimpleHeaderComponent, SimpleHeaderToolsComponent } from './components/table-simple-header/table-simple-header.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { AdvancedSelectComponent } from './components/advanced-select/advanced-select.component';
import { AdvancedSelectService } from './components/advanced-select/advanced-select.service';
import { AdvancedTableEditorDirective } from './directives/advanced-table-editor.directive';
import {AdvancedAggregatorComponent} from "./components/advanced-aggregator/advanced-aggregator.component";
import {AdvancedAggregatorListComponent} from "./components/advanced-aggregator-list/advanced-aggregator-list.component";
import {AdvancedAggregatorService} from './services/advanced-aggregator/advanced-aggregator.service';
import {AdvancedAggregatorPipe} from './pipes/advanced-aggregator.pipe';
import {GroupByPipe, NgPipesModule} from 'ngx-pipes';
import {ParseTitlePipe} from './pipes/advansed-aggregator-list-title-parse.pipe';
import {AddItemsComponent} from "./components/add-items/add-items.component";
import { AdvancedSearchState } from './directives/advance-search-state.directive';
import { AdvancedTableState } from './directives/advance-table-state.directive';
import { AdvancedSearchRouterState } from './directives/advanced-search-router-state.directive';
import { AdvancedTableSearchBaseComponent } from './components/advanced-table/advanced-table-search-base';
import { ColumnSelectorComponent } from './components/column-selector/column-selector.component';
import { ColumnSelectorService } from './components/column-selector/column-selector.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        RouterModalModule,
        FormioModule,
        AdvancedFormsModule,
        SharedModule,
        ModalModule,
        ProgressbarModule,
        NgPipesModule
    ],
    providers: [DatePipe, TemplatePipe,
      {
        provide: COLUMN_FORMATTERS,
        useValue: FORMATTERS
      },

      AdvancedTableConfigurationResolver,
        AdvancedSearchConfigurationResolver,
        AdvancedSelectService,
        AdvancedAggregatorService,
        ColumnSelectorService,
        GroupByPipe
      ],
    declarations: [
        AdvancedTableComponent,
        AdvancedTableSearchBaseComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedTableModalBaseComponent,
        AdvancedSearchFormComponent,
        AdvancedListComponent,
        AdvancedRowActionComponent,
        TableSimpleHeaderComponent,
        SimpleHeaderToolsComponent,
        AdvancedSelectComponent,
        AdvancedTableEditorDirective,
        AdvancedAggregatorComponent,
        AdvancedAggregatorListComponent,
        AdvancedAggregatorPipe,
        ParseTitlePipe,
        AddItemsComponent,
        AdvancedSearchState,
        AdvancedTableState,
        AdvancedSearchRouterState,
        ColumnSelectorComponent
    ],
    exports: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedSearchFormComponent,
        AdvancedListComponent,
        AdvancedRowActionComponent,
        TableSimpleHeaderComponent,
        SimpleHeaderToolsComponent,
        AdvancedSelectComponent,
        AdvancedTableEditorDirective,
        AdvancedAggregatorComponent,
        AdvancedAggregatorListComponent,
        AddItemsComponent,
        AdvancedSearchState,
        AdvancedTableState,
        AdvancedSearchRouterState,
        ColumnSelectorComponent
    ],
    entryComponents: [
        AdvancedTableModalBaseComponent,
        AdvancedTableSettingsComponent,
        AdvancedRowActionComponent,
        AdvancedSelectComponent,
        AddItemsComponent,
        ColumnSelectorComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TablesModule implements OnInit {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading tables module');
            console.error(err);
        });
    }

    async ngOnInit() {
        this._translateService.setTranslation('en', TABLES_EN, true);
        this._translateService.setTranslation('el', TABLES_EL, true);
    }

}
